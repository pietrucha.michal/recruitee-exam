import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DefaultLayoutComponent} from "./share/layout/default-layout/default-layout.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/list' },
  {path: 'list', loadChildren: () => import('./core/core.module').then(m => m.CoreModule), component: DefaultLayoutComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
