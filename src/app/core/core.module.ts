import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { ListComponent } from './components/list/list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AddDataComponent } from './components/add-data/add-data.component';
import { EditDataComponent } from './components/edit-data/edit-data.component';
import {ShareModule} from "../share/share.module";
import { DetailsComponent } from './components/details/details.component';


@NgModule({
  declarations: [
    ListComponent,
    AddDataComponent,
    EditDataComponent,
    DetailsComponent,
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    FormsModule, ReactiveFormsModule, ShareModule
  ]
})
export class CoreModule { }
