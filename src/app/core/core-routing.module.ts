import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from "./components/list/list.component";
import {AddDataComponent} from "./components/add-data/add-data.component";
import {EditDataComponent} from "./components/edit-data/edit-data.component";
import {DetailsComponent} from "./components/details/details.component";

const routes: Routes = [
  {path: '', component: ListComponent},
  {path: 'add', component: AddDataComponent},
  {path: ':uuid/edit', component: EditDataComponent},
  {path: ':uuid/details', component: DetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
