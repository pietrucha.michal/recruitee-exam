import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {mergeMap, tap} from "rxjs";
import {DataService} from "../../../share/services/data.service";
import {DataModel} from "../../../share/models/data.model";
import {Location} from "@angular/common";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  uuid: string = String();
  data!: DataModel;

  constructor(private readonly _route: ActivatedRoute,
              private readonly _dataService: DataService,
              private readonly location: Location,
              private readonly router: Router) {
  }

  ngOnInit(): void {
    this.listenOnRouteChange();
  }

  back(): void {
    this.location.back();
  }

  private listenOnRouteChange(): void {
    this._route.params
      .pipe(
        mergeMap((params: Params) => {
          this.uuid = params['uuid'] || String();
          return this._dataService.data;
        }),
        tap((data: DataModel[]) => {
          const row = data.find(e => e.id === this.uuid);

          if(!row) {
            this.router.navigate(['/list']);
            return;
          }

          this.data = row;

        })
      )
      .subscribe()
  }
}
