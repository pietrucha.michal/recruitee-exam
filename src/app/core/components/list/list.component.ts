import {Component, Injector, OnInit} from '@angular/core';
import {DataService} from "../../../share/services/data.service";
import {ActivatedRoute, Params} from "@angular/router";
import {FilterList} from "../../../share/utils/filter.abtract";
import {DataModel} from "../../../share/models/data.model";
import {removeEmptyFieldsObject} from "../../../share/utils/empty-fields.utils";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends FilterList implements OnInit {
  filter: string = String();
  listData: DataModel[] = [];
  total: number = 0;
  pages: number[] = [];
  readonly paginate: number[] = [5, 10, 20, 50, 75, 100];
  headers = TABLE_HEADERS;

  constructor(private readonly _dataService: DataService,
              private readonly _route: ActivatedRoute,
              injector: Injector
  ) {
    super(injector, TABLE_HEADERS);
  }

  ngOnInit(): void {
    this.listenOnRouteChanges();
  }

  handleFilterSearch($event: any): void {
    this.handleFilters(removeEmptyFieldsObject<{ text: string }>($event));
  }

  init(params: Params): void {
    this._dataService.getAndPaginate(params).subscribe(data => {
      this.listData = data.data;
      this.total = data.total;
      this.getPages(data.total);
      this.setText();
    })
  }

  delete(uuid: string, $event: MouseEvent): void {
    $event.stopPropagation();
    $event.preventDefault();
    if (window.confirm('are you sure ?')) {
      this._dataService.delete(uuid);
    }
  }

  onFilterChange($event: string) {
    this.handleFilterSearch({text: $event});
  }

  private listenOnRouteChanges(): void {
    this._route.queryParams
      .subscribe((params: Params) => {
        this.init(params);
        this.handleSortField(params);
      });
  }

  private getPages(pages: number): void {
    this.pages = [];
    for (let i = 1; i <= this.getAvailablePages(pages); i++) {
      this.pages.push(i);
    }
  }

  private setText(): void {
    this.filter = this._route.snapshot.queryParams['text'] || '';
  }
}

export const TABLE_HEADERS: Array<{ type: string; status: number; show: boolean }> = [
  {type: 'Avatar', status: 0, show: false},
  {type: 'Name', status: 0, show: true},
  {type: 'Company', status: 0, show: true},
  {type: 'Actions', status: 0, show: false},
]
