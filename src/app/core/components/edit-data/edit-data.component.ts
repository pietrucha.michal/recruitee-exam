import {Component, OnInit} from '@angular/core';
import {DataService} from "../../../share/services/data.service";
import {ActivatedRoute, Params, Route, Router} from "@angular/router";
import {mergeMap, tap} from "rxjs";
import {DataModel} from "../../../share/models/data.model";
import {Location} from "@angular/common";

@Component({
  selector: 'app-edit-data',
  templateUrl: './edit-data.component.html',
  styleUrls: ['./edit-data.component.scss']
})
export class EditDataComponent implements OnInit {
  data!: DataModel;
  private uuid: string = String();

  constructor(private readonly _dataService: DataService,
              private readonly _route: ActivatedRoute,
              private readonly _router: Router,
              private location: Location){
  }

  ngOnInit(): void {
    this.listenOnUrlChanges();
  }

  private listenOnUrlChanges(): void {
    this._route.params
      .pipe(
        mergeMap((params: Params) => {
          this.uuid = params['uuid'] || String();

          return this._dataService.data;
        }),
        tap((data: DataModel[]) => {
          const row = data.find(e => e.id === this.uuid);
          if (!row) {
            this._router.navigate(['/list']);
            return;
          }
          this.data = row;
        })
      )
      .subscribe();
  }

  updateRow($event: Partial<DataModel>) {
    const final: DataModel = {...this.data, ...$event}
    this._dataService.update(final);
    this.location.back();
  }
}
