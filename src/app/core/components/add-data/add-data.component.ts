import { Component, OnInit } from '@angular/core';
import {DataModel} from "../../../share/models/data.model";
import {DataService} from "../../../share/services/data.service";
import {GenerateUuid} from "../../../share/utils/generate-uuid.utils";

@Component({
  selector: 'app-add-data',
  templateUrl: './add-data.component.html',
  styleUrls: ['./add-data.component.scss']
})
export class AddDataComponent implements OnInit {

  constructor(private _dataService: DataService) { }

  ngOnInit(): void {
  }

  addRow($event: Partial<DataModel>) {
    this._dataService.add($event);
  }
}
