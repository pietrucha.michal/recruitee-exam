import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { initData } from './utils/init-data.utils';
import { DataService } from './services/data.service';
import { DefaultLayoutComponent } from './layout/default-layout/default-layout.component';
import {RouterModule} from "@angular/router";
import { AddEditDataComponent } from './components/add-edit-data/add-edit-data.component';
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    DefaultLayoutComponent,
    AddEditDataComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  exports: [
    AddEditDataComponent
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initData,
      deps: [DataService],
      multi: true,
    }
  ]
})
export class ShareModule { }
