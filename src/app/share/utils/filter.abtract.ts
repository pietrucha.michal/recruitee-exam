import {ActivatedRoute, Params, Router} from "@angular/router";
import {Injector} from "@angular/core";
import {Subject} from "rxjs";

export interface FilterOptionsModel {
  params: Object;
  size: number;
  page: number;
}

export interface TableHeaders {
  status: number,
  type: string,
}

export abstract class FilterList {
  public filterOptions: FilterOptionsModel = {params: {}, size: 20, page: 1};
  public tableHeaders: Array<TableHeaders>;
  public sortBy: { type: 'ASC' | 'DESC', field: TableHeaders } | null = null;
  public router: Router;
  public route: ActivatedRoute;

  protected constructor(injector: Injector, headers: Array<TableHeaders>) {
    this.router = injector.get(Router);
    this.route = injector.get(ActivatedRoute);
    this.tableHeaders = headers;
    this.setPageAndSize();
  }

  public abstract init(params: Params): void;

  public handleFilters($event: any) {
    this.filterOptions = {...this.filterOptions, params: $event, page: 1};
    this.applyFilters();
  }

  public onPageSizeChange($event: number) {
    console.log($event);
    this.filterOptions.size = $event;
    this.filterOptions.page = 1;
    this.applyFilters();
  }

  public onPageIndexChange($event: number) {
    this.filterOptions.page = $event;
    this.applyFilters();
  }

  public getAvailablePages(total: number) {
    return Math.ceil(total/this.filterOptions.size);
  }

  public handleSort(field: TableHeaders) {
    const reassign = (reset: boolean = false) => {
      if (reset) {
        field.status = 0;
        this.sortBy!.field.status = 0;
      }

      this.sortBy = {field: field, type: 'ASC'};
    }

    const handleNext = () => {
      if (this.sortBy!.field.status >= 2) {
        this.sortBy!.field.status = 0;
        this.sortBy = null;
      } else {
        this.sortBy!.field.status++;
      }
    }

    if (!this.sortBy) {
      reassign();
    }

    if (this.sortBy!.field.type === field.type) {
      handleNext();
    } else {
      reassign(true);
      this.sortBy!.field.status++;
    }

    this.applyFilters();
  }

  public handleSortField($event: Params) {
    if (!$event['sort']) {
      return;
    }

    const sort = JSON.parse($event['sort']).sort;
    if (sort || sort.length >= 2) {
      const field = sort[0];
      const direction = sort[1];

      const row = this.tableHeaders.find(e => e.type === field);

      for (const header of this.tableHeaders) {
        if (header.type === row?.type) {
          continue;
        }
        header.status = 0;
      }

      if (!row) {
        return;
      }

      switch (direction) {
        case 'ASC':
          row.status = 1;
          break
        case 'DESC':
          row.status = 2;
      }
    }
  }

  private sortDirection(): 'ASC' | 'DESC' | void {
    switch (this.sortBy!.field.status) {
      case 1:
        return 'ASC'
      case 2:
        return 'DESC'
    }
  }

  private applyFilters(): void {
    let sortTo;
    if (this.sortBy) {
      sortTo = {sort: [this.sortBy.field.type, this.sortDirection()]}
    }

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        ...this.filterOptions.params,
        size: this.filterOptions.size,
        page: this.filterOptions.page,
        sort: JSON.stringify(sortTo)
      }, queryParamsHandling: '',
    });
  }

  private setPageAndSize(): void {
    const {size, page} = this.route.snapshot.queryParams;

    if(page) {
      this.filterOptions.page = page;
    }

    if(size) {
      this.filterOptions.size = size;
    }
  }
}
