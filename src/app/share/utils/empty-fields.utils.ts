export function removeEmptyFieldsObject<T>(from: object): Partial<T> {
  for (const [key, value] of Object.entries(from)) {
    if (value === null || value === undefined || value === '')  {
      // @ts-ignore
      delete from[key];
    }
  }

  return from;
}
