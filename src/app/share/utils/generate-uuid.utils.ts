export function GenerateUuid(): string {
  const chars: string = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
  let final = String();

  for(let i = 0; i < 24; i++) {
    final += chars.charAt(Math.floor(Math.random() * chars.length))
  }

  return final;
}
