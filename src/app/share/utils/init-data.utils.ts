import { DataService } from "../services/data.service";

export function initData(dataService: DataService) {
    return () => dataService.setDataOnInit();
}