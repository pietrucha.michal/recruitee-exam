import {Injectable} from '@angular/core';
import {Router, RoutesRecognized} from "@angular/router";
import {filter, pairwise} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RouterHelperService {
  previousRoute: string = String();

  constructor(private router: Router) {
  }

  public listenOnChanges(): void {
    this.router.events.pipe(
      filter((evt: any) => evt instanceof RoutesRecognized),
      pairwise()
    ).subscribe((events: RoutesRecognized[]) => {
      this.previousRoute = events[0].urlAfterRedirects;
    });
  }
}
