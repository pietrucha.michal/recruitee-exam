import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {DataModel} from "../models/data.model";
import {BehaviorSubject, catchError, delay, findIndex, map, Observable, take, tap, throwError} from "rxjs";
import {Data, Params} from "@angular/router";
import {PaginateModel} from "../models/paginate.model";
import {GenerateUuid} from "../utils/generate-uuid.utils";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _data$: BehaviorSubject<DataModel[]> = new BehaviorSubject<DataModel[]>([]);

  constructor(private http: HttpClient) {
  }

  public get data() {
    return this._data$;
  }

  private set setData(data: DataModel[]) {
    this._data$.next(data);
  }

  public setDataOnInit(): Observable<DataModel[]> {
    return this.http.get<DataModel[]>('/assets/mock/data.json')
      .pipe(
        delay(0),
        take(1),
        catchError((err: HttpErrorResponse) => {
          console.error(err);
          console.trace();
          return throwError(() => err);
        }),
        tap((data: DataModel[]) => this.setData = data)
      );
  }

  public add($event: Partial<DataModel>): void {
    $event.id = GenerateUuid();
    $event.createdAt = Date.now() / 1000;

    const list = this.data.getValue();

    list.push($event as DataModel);

    this.setData = list;
  }

  public update(data: DataModel): void {
    const list = this.data.getValue().slice();
    const rowIndex: number = list.findIndex(e => e.id === data.id);

    if(rowIndex < 0) {
      return;
    }

    list[rowIndex] = data;

    this.setData = list;
  }

  public delete(uuid: string): void {
    const data = this.data.getValue().slice();
    const index = data.findIndex(e => e.id === uuid);

    if(index < 0) {
      return;
    }

    data.splice(index, 1);

    this.setData = data;
  }

  public getAndPaginate(params: Params): Observable<PaginateModel<DataModel[]>> {
    return this.data.pipe(
      map((_data) => {
        const {text} = params;

        if(text === undefined) {
          return _data;
        }

        return _data
          .filter(row => row.name.toLowerCase().includes(text) || row.company.toLowerCase().includes(text))
      }),
      map((_data) => {
        const data = _data.slice();
        const {sort} = params;

        if (!sort) {
          return data;
        }
        let sortObj;
        sortObj = JSON.parse(sort);
        const field: keyof DataModel = (sortObj.sort[0]).toLowerCase();
        const direction: 'ASC' | 'DESC' = sortObj.sort[1];
        let firstCompare = direction === 'ASC' ? 1 : -1;
        let secondCompare = direction === 'ASC' ? -1 : 1;


        data.sort((a, b) => (a[field] > b[field]) ? firstCompare : secondCompare);

        return data;
      }),
      map(data => {
        const page = params['page'] ? params['page'] : 1;
        const size = params['size'] ? params['size'] : 20;

        const total = data.length;

        data = data.slice().splice((page - 1) * size, size);

        return {
          data,
          total: total
        }
      })
    )
  }
}

