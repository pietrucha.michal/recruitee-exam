export interface DataModel {
  id: string;
  avatarUrl: string;
  name: string;
  company: string;
  email: string;
  phone: string;
  address: string;
  about: string;
  createdAt: number;
  tags: string[];
}
