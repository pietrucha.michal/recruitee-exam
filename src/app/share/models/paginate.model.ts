export interface PaginateModel<T> {
  data: T;
  total: number;
}
