export enum AddEditDataFormEnum {
  AVATAR_URL = 'avatarUrl',
  NAME = 'name',
  COMPANY = 'company',
  EMAIL = 'email',
  PHONE = 'phone',
  ADDRESS = 'address',
  ABOUT = 'about',
  TAGS = 'tags'
}
