import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {AddEditDataFormEnum} from "./models/add-edit-data.enum";
import {DataModel} from "../../models/data.model";
import {Location} from "@angular/common";
import {RouterHelperService} from "../../services/router-helper.service";

@Component({
  selector: 'app-add-edit-data',
  templateUrl: './add-edit-data.component.html',
  styleUrls: ['./add-edit-data.component.scss']
})
export class AddEditDataComponent implements OnInit {
  form!: FormGroup;
  addEditDataFormEnum: typeof AddEditDataFormEnum = AddEditDataFormEnum;

  @Input()
  public data: DataModel | null = null;

  @Output('onSubmitEvent')
  public submitEvent: EventEmitter<Partial<DataModel>> = new EventEmitter<Partial<DataModel>>();

  constructor(private formBuilder: FormBuilder,
              private location: Location,
              private readonly _routerHelperService: RouterHelperService
  ) {
  }

  get getTagsForm() {
    return (this.form.get(this.addEditDataFormEnum.TAGS) as FormArray);
  }

  ngOnInit(): void {
    this.initForm();

    if (this.data) {
      this.patchForm();
    }
  }

  onSubmit() {
    if(this.form.invalid) {
      return;
    }

    this.submitEvent.next(this.form.getRawValue());
    this.back();
  }

  getControlErrors(controlName: AddEditDataFormEnum, errorCode: string): boolean | undefined {
    return this.form.get(controlName)?.hasError(errorCode) && this.form.get(controlName)?.touched;
  }

  back() {
    this.location.back();
  }

  add() {
    this.getTagsForm.push(this.generateFormId());
  }

  delete(index: number) {
    this.getTagsForm.removeAt(index);
  }

  private patchForm(): void {
    this.form.patchValue(this.data as DataModel);

    for (const tag of this.data?.tags as string[]) {
      (this.form.get(this.addEditDataFormEnum.TAGS) as FormArray).push(this.generateFormId(tag))
    }
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      [this.addEditDataFormEnum.AVATAR_URL]: ['http://placehold.it/32x32',
        {validators: [Validators.required], updateOn: 'blur'}],
      [this.addEditDataFormEnum.NAME]: [null, [Validators.required]],
      [this.addEditDataFormEnum.COMPANY]: [null, [Validators.required]],
      [this.addEditDataFormEnum.EMAIL]: [null, [Validators.required, Validators.email]],
      [this.addEditDataFormEnum.PHONE]: [null, [
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(14)]],
      [this.addEditDataFormEnum.ADDRESS]: [null, Validators.required],
      [this.addEditDataFormEnum.ABOUT]: [null, Validators.required],
      [this.addEditDataFormEnum.TAGS]: this.formBuilder.array([]),
    })
  }

  private generateFormId(value: string = String()): FormControl {
    return new FormControl(value);
  }
}
