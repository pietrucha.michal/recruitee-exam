import { Component } from '@angular/core';
import {RouterHelperService} from "./share/services/router-helper.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(routeHelperService: RouterHelperService) {
    routeHelperService.listenOnChanges();
  }
}
